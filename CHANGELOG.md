# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.13] - 2023-06-15
* Further work on Cloudwatch permission error.

## [1.0.12] - 2023-06-15
* Fix for Cloudwatch permission error.

## [1.0.11] - 2023-06-15
* Upgrade CLAWS dependency

## [1.0.10] - 2023-05-10
* Upgrade CLAWS dependency

## [1.0.9] - 2023-05-02
* Add option to sign AWS requests

## [1.0.8] - 2023-05-02
* Fetch AWS credentials from environment variables if present

## [1.0.7] - 2023-04-27
* Add helper function add_counter to quickly add counters

## [1.0.6] - 2023-04-23
* Internal bugfixes

## [1.0.5] - 2023-04-25
* Allow user to override instrumentation parameter (useful for testing)
* Better error handling

## [1.0.4] - 2023-04-20
* Bump CLAWS dependency

## [1.0.3] - 2023-03-06
* Fixes to metric logging

## [1.0.2] - 2023-03-03
* Add group name and stream name to decorator

## [1.0.1] - 2023-03-03
* Add ability to specify group name and stream name in CloudWatch

## [1.0.0] - 2023-03-31
* Remove aws decorator and use single @instrument decorator
* Add ability to push logs to CloudWatch using watchtower

## [0.0.1] - 2023-03-30
* First commit